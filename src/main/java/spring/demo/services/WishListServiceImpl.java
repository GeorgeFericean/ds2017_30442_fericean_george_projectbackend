package spring.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.entities.Product;
import spring.demo.entities.Wishlist;
import spring.demo.repositories.ProductRepository;
import spring.demo.repositories.WishlistRepository;

@Service
public class WishListServiceImpl implements WishListService {
	private ProductRepository productRepository;
	private WishlistRepository wishlistRepository;
	
	@Autowired
	public WishListServiceImpl(ProductRepository productRepository, WishlistRepository wishlistRepository) {
		this.productRepository = productRepository;
		this.wishlistRepository = wishlistRepository;
	}
	 @Override
	    public Wishlist addToWishlist(int prodId, int userId) {
	        Wishlist wishlist=new Wishlist(userId,prodId);

	        return wishlistRepository.save(wishlist);
	    }

	    @Override
	    public void removeFromWishlist(int prodId, int userId) {
	        wishlistRepository.delete(userId,prodId);
	    }

	    @Override
	    public List<Product> getWishlist(int id) {
	        List<Wishlist> wishlist=wishlistRepository.findByUserId(id);
	        List<Product> wishlistProducts=new ArrayList<>();
	        for(Wishlist prod:wishlist)
	        {
	            Product product=productRepository.findById(prod.getProductId());
	            wishlistProducts.add(product);
	        }
	        return wishlistProducts;
	    }
}
