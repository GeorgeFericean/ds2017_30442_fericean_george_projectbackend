package spring.demo.services;

import java.util.List;

import spring.demo.entities.Product;
import spring.demo.entities.Wishlist;

public interface WishListService {
	  Wishlist addToWishlist(int prodId, int userId);
	    void removeFromWishlist(int prodId, int userId);
	    List<Product> getWishlist(int id);
}
