package spring.demo.services;



import java.util.List;

import spring.demo.entities.PromoCode;
import spring.demo.entities.User;

/**
 * Created by George on 4/18/2017.
 */
public interface UserService {
    List<User> getAll();
    User create(User user);
    User findByUsername(String username);
    List<User> findByName(String name);
    User findByEmail(String email);
    User findById(int id);
    void update(User user,int id);
    void delete(int id);
   
    


}
