package spring.demo.services;

import java.util.List;

import spring.demo.entities.Brand;
import spring.demo.entities.Category;
import spring.demo.entities.Product;
import spring.demo.entities.Wishlist;


public interface ProductService {
	 List<Product> getAll();
	    List<Product> filter(String brand,String category, float priceMin, float priceMax);
	    List<Product> searchProducts(String searchInput);
	    
	    Product create(Product product);
	    Product findById(int id);
	    Product findByType(String type);
	    void update(Product product,int id);
	    void delete(int id);
	    
	    List<Brand> getAllBrands();
	    Brand createBrand(Brand brand);
	    List<Category> getAllCategories();
	    Category createCategory(Category category);
	   
	  
}
