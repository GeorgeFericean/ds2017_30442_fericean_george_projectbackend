 package spring.demo.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.entities.PromoCode;
import spring.demo.entities.User;
import spring.demo.entities.UserRole;
import spring.demo.repositories.PromoCodeRepository;
import spring.demo.repositories.UserRepository;
import spring.demo.repositories.UserRoleRepository;

import java.util.List;

/**
 * Created by George on 4/18/2017.
 */
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;
   
    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserRoleRepository userRoleRepository)
    {
        this.userRepository=userRepository;
        this.userRoleRepository=userRoleRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(User user)  {
        //PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        //user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserRole userRole=new UserRole(user,"USER");
        userRepository.save(user);
        userRoleRepository.save(userRole);
        return user;
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    @Override
    public User findById(int id) {
        return userRepository.findById(id);
    }

    @Override
    public void update(User user, int id) {
      //  PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
       userRepository.update(id, user.getUsername(), user.getPassword(),
    		   user.getName(),user.getEmail(),
    		   user.getAddress(),user.getCity(),user.getCountry(),
    		   user.getTelephone(),user.getIBAN());
    }

    @Override
    public void delete(int id) {
    	User user=userRepository.findById(id);
    	userRoleRepository.delete(user);
         userRepository.delete(id);
    }

	@Override
	public List<User> findByName(String name) {
		return userRepository.findByName(name);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

}
