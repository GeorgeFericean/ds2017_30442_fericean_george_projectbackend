package spring.demo.services;

import java.util.List;

import spring.demo.entities.Message;

public interface MessageService {
	Message sendMessage(Message message);
	List<Message> getAllMessagesForUser(String username);
	void deleteMessage(int id);
}
