package spring.demo.services;

import java.util.List;

import spring.demo.entities.PromoCode;

public interface PromoCodeService {
	 PromoCode giftPromoCode(PromoCode promoCode);
	List<PromoCode> getAllPromoCodes(String username);
	void deletePromoCode(String code);
	PromoCode findByCode(String code);
}
