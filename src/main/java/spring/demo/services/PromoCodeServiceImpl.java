package spring.demo.services;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.entities.PromoCode;
import spring.demo.repositories.PromoCodeRepository;
@Service
public class PromoCodeServiceImpl implements PromoCodeService{
	 private PromoCodeRepository promoCodeRepository;
	 
	 @Autowired
	 public PromoCodeServiceImpl(PromoCodeRepository promoCodeRepository) {
		 this.promoCodeRepository=promoCodeRepository;
	 }
	@Override
    public PromoCode giftPromoCode( PromoCode promoCode) {
        return promoCodeRepository.save(promoCode);
    }

    @Override
    public List<PromoCode> getAllPromoCodes(String username) {
        return promoCodeRepository.findByUsername(username);
    }
 
	@Override
	public void deletePromoCode(String code) {
		promoCodeRepository.delete(code);
		
	}
	  
	@Override
	public PromoCode findByCode(String code) {
		return promoCodeRepository.findByCode(code);
	}
}
