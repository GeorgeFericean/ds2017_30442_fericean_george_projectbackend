package spring.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.entities.Message;
import spring.demo.repositories.MessageRepository;

@Service
public class MessageServiceImpl implements MessageService{
	private MessageRepository messageRepository;
	@Autowired
	public MessageServiceImpl(MessageRepository messageRepository) {
		this.messageRepository=messageRepository;
	}
	@Override
	public Message sendMessage(Message message) {
		return messageRepository.save(message);
	}

	@Override
	public List<Message> getAllMessagesForUser(String username) {
		return messageRepository.findByUsername(username);
	}

	@Override
	public void deleteMessage(int id) {
		messageRepository.delete(id);
		
	}

}
