package spring.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.entities.Brand;
import spring.demo.entities.Category;
import spring.demo.entities.Product;
import spring.demo.entities.ProductTag;
import spring.demo.entities.Wishlist;
import spring.demo.repositories.BrandRepository;
import spring.demo.repositories.CategoryRepository;
import spring.demo.repositories.ProductRepository;
import spring.demo.repositories.TagRepository;
import spring.demo.repositories.WishlistRepository;
@Service
public class ProductServiceImpl implements ProductService {
	private ProductRepository productRepository;
	private BrandRepository brandRepository;
	private CategoryRepository categoryRepository;
	private TagRepository tagRepository;
	

	@Autowired
	public ProductServiceImpl(ProductRepository productRepository, BrandRepository brandRepository,
			CategoryRepository categoryRepository, TagRepository tagRepository) {

		this.productRepository = productRepository;
		this.brandRepository = brandRepository;
		this.categoryRepository = categoryRepository;
		this.tagRepository = tagRepository;
	}
	 public List<Product> getAll() {
	        return productRepository.findAll();
	    }

	    @Override
	    public List<Product> searchProducts(String searchInput) {
	        List<Product> products=productRepository.findAll();
	        for(Product prod:products)
	        {
	        	ProductTag tag=tagRepository.findByProduct(prod.getId());
	            boolean found=tag.getTags().toLowerCase().contains(searchInput.toLowerCase());
	            if(!found)
	            	products.remove(prod);
	        }

	        return products;
	    }

	    @Override
	    public Product create(Product product) {
	        Product prod=new Product(product.getBrand(),product.getCategory(),
	                product.getType(),product.getColor(),product.getPrice(),product.getQuantity());
	        String tags=""+prod.getBrand()+" "+prod.getCategory()+" "+prod.getType()+" "+prod.getColor();
	        Product prodSaved=productRepository.save(prod);
	        ProductTag tag=new ProductTag(prodSaved.getId(),tags);
	         tagRepository.save(tag);

	        return prod;
	    }

	    @Override
	    public void update(Product product,int id) {
	        productRepository.update(id,product.getBrand(),product.getCategory(),product.getType(),product.getColor(),
	                product.getQuantity(),product.getPrice());

	        ProductTag tag=tagRepository.findByProduct(id);
	        String tags=""+product.getBrand()+" "+product.getCategory()+" "+product.getType()+" "+product.getColor();
	        tagRepository.update(tag.getId(),tags);
	    }

	    @Override
	    public void delete(int id) {
	        productRepository.delete(id);

	    }

	    @Override
	    public Category createCategory(Category category) {
	        return categoryRepository.save(category);
	    }

	    @Override
	    public Brand createBrand(Brand brand) {
	        return brandRepository.save(brand);
	    }

	   

	    @Override
	    public Product findById(int id) {
	        return productRepository.findOne(id);
	    }

	    @Override
	    public List<Brand> getAllBrands() {
	        return brandRepository.findAll();
	    }

	    @Override
	    public List<Category> getAllCategories() {
	        return categoryRepository.findAll();
	    }
		@Override
		public Product findByType(String type) {
			return productRepository.findByType(type);
		}
		@Override
		public List<Product> filter(String brand, String category, float priceMin, float priceMax) {
			List<Product> productsFiltered=productRepository.findAll();
			for(Product product: productsFiltered) {
				if(product.getBrand()!=brand || product.getCategory()!=category || (product.getPrice()<priceMin || product.getPrice()>priceMax)) {
					productsFiltered.remove(product);
				}
			}
			return productsFiltered;
		}
}
