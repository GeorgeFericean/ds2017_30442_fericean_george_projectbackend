package spring.demo.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.PromoCode;

import java.util.List;

/**
 * Created by George on 5/23/2017.
 */
public interface PromoCodeRepository extends JpaRepository<PromoCode,Integer> {
    List<PromoCode> findByUsername(String username);
    PromoCode findByCode(String code);
    @Modifying
    @Transactional
    @Query("delete from PromoCode u where u.code = :code")
    void delete(@Param("code")String code);
}
