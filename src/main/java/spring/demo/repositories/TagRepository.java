package spring.demo.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.ProductTag;

/**
 * Created by George on 5/23/2017.
 */
public interface TagRepository extends JpaRepository<ProductTag,Integer>{
        ProductTag findByProduct(int product);
         @Modifying
         @Transactional
         @Query("UPDATE ProductTag t SET t.tags = :tags WHERE t.id = :id")
        void update(@Param("id") int id, @Param("tags") String newTags);
}
