package spring.demo.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.Product;

import java.util.List;

/**
 * Created by George on 5/18/2017.
 */
public interface ProductRepository extends JpaRepository<Product,Integer> {
		Product findById(int id);
        List<Product> findByCategory(String category);
        List<Product> findByBrand(String brand);
        Product findByType(String type);
        @Modifying
        @Transactional
        @Query("UPDATE Product p SET p.brand = :brand, p.category= :category," +
                " p.type= :type, p.color= :color, p.quantity= :quantity" +
                ", p.price= :price WHERE p.id = :id")
        void update(@Param("id") int id, @Param("brand") String newBrand,@Param("category") String newCategory,
                    @Param("type") String newType,@Param("color") String newColor,
                    @Param("quantity") long newQuantity,@Param("price") float newPrice);
}
