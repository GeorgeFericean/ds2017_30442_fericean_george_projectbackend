package spring.demo.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.User;
import spring.demo.entities.UserRole;

/**
 * Created by George on 4/22/2017.
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {
	@Modifying
    @Transactional
    @Query("delete from UserRole u where u.user = :user")
    void delete(@Param("user")User user);
}
