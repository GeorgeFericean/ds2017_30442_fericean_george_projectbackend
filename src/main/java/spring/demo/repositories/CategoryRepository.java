package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Category;

/**
 * Created by George on 5/18/2017.
 */
public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
