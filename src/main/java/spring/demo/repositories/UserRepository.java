package spring.demo.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.User;

/**
 * Created by George on 4/16/2017.
 */
public interface UserRepository  extends JpaRepository<User, Integer> {
    User findByUsername(String username);
    List<User> findByName(String name);
    User findByEmail(String email);
    User findById(int id);
    @Modifying
    @Transactional
    @Query("UPDATE User u SET u.username = :username,"
    		+ " u.password= :password,"
    		+ " u.name = :name,"
    		+ " u.email = :email,"
    		+ " u.address = :address,"
    		+ " u.city = :city,"
    		+ " u.country = :country,"
    		+ " u.telephone = :telephone,"
    		+ " u.IBAN = :IBAN"
    		+ " WHERE u.id = :id")
    void update(@Param("id") int id,
    		@Param("username") String newUsername,
    		@Param("password") String newPassword,
    		@Param("name") String newName,
    		@Param("email") String newEmail,
    		@Param("address") String newAddress,
    		@Param("city") String newCity,
    		@Param("country") String newCountry,
    		@Param("telephone") String newTelephone,
    		@Param("IBAN") String newIBAN
    		);
    @Modifying
    @Transactional
    @Query("delete from User u where u.id = :id")
    void delete(@Param("id")int id);

}
