package spring.demo.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Brand;

/**
 * Created by George on 5/18/2017.
 */
public interface BrandRepository extends JpaRepository<Brand,Integer> {
}
