package spring.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.Message;

public interface MessageRepository extends JpaRepository<Message,Integer> {
	List<Message> findByUsername(String username);
	@Modifying
    @Transactional
    @Query("delete from Message u where u.id = :id")
    void delete(@Param("id")int id);
}
