package spring.demo.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import spring.demo.entities.Wishlist;

import java.util.List;

/**
 * Created by George on 5/26/2017.
 */
public interface WishlistRepository extends JpaRepository<Wishlist,Integer> {

    List<Wishlist> findByUserId(int id);
    @Modifying
    @Transactional
    @Query("delete from Wishlist w where w.userId = :userId and w.productId= :productId")
    void delete(@Param("userId") int userId,@Param("productId") int productId);

}