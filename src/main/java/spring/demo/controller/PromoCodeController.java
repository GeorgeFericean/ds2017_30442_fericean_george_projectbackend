package spring.demo.controller;


import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.entities.Message;
import spring.demo.entities.PromoCode;
import spring.demo.services.MessageService;
import spring.demo.services.PromoCodeService;
import spring.demo.services.UserService;

@RestController
public class PromoCodeController {
	@Autowired
	private PromoCodeService promoCodeService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private UserService userService;
	 protected String getCodeString() {
	        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	        StringBuilder salt = new StringBuilder();
	        Random rnd = new Random();
	        while (salt.length() < 7) { // length of the random string.
	            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
	            salt.append(SALTCHARS.charAt(index));
	        }
	        String saltStr = salt.toString();
	        return saltStr;
	    }
	public String generatePromoCodeMessageContent(PromoCode promoCode) {
		String content="Congratulations "+promoCode.getUsername()+"!\n";
		content+="You have been awarded a promotional code that you can use to get discounts on purchases\n";
		content+="The details are shown below:\n";
		content+="promo code ID: "+promoCode.getCode()+"\n";
		content+="Discount: "+promoCode.getDiscount()*100+"%\n";
		return content;
	}
	@RequestMapping(value = "/promo/create", method = RequestMethod.POST)
	public String sendPromoCode(@RequestBody PromoCode promoCode) {
		if(userService.findByUsername(promoCode.getUsername())==null) {
			return "The user was not found";
		}
		promoCode.setCode(getCodeString());
		promoCodeService.giftPromoCode(promoCode);
		Message message=new Message();
		message.setUsername(promoCode.getUsername());
		message.setContent(generatePromoCodeMessageContent(promoCode));
		messageService.sendMessage(message);
		return "Promotional Code sent";
	}
	@RequestMapping(value="/promo/delete/{code}", method=RequestMethod.POST)
	public void deletePromoCode(@PathVariable("code") String code){
		promoCodeService.deletePromoCode(code);
	}
	@RequestMapping(value="/promo/get/{code}", method=RequestMethod.GET)
	public PromoCode getPromoCode(@PathVariable("code") String code){
		return promoCodeService.findByCode(code);
	}
}
