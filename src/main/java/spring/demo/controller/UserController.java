package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import spring.demo.entities.User;
import spring.demo.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users/getByID/{id}", method = RequestMethod.GET)
	public User getUserById(@PathVariable int id) {
		return userService.findById(id);
	}
	@RequestMapping(value = "/users/getByName/{name}", method = RequestMethod.GET)
	public List<User> getUsersByName(@PathVariable String name) {
		return userService.findByName(name);
	}
	@RequestMapping(value = "/users/getByUsername/{username}", method = RequestMethod.GET)
	public User getUserByUsername(@PathVariable("username") String username) {
		return userService.findByUsername(username);
	}
	@RequestMapping(value = "/users/getByEmail/{email}", method = RequestMethod.GET)
	public User getUserByEmail(@PathVariable("email") String email) {
		return userService.findByEmail(email);
	}
	@RequestMapping(value = "/users/all", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userService.getAll();
	}

	@RequestMapping(value = "/users/create", method = RequestMethod.POST)
	public String insertUser(@RequestBody User user) {
		if(userService.findByEmail(user.getEmail())!=null ||
				userService.findByUsername(user.getUsername())!=null) {
				return "Username or E-mail are already in use";
				}
		try {
				userService.create(user);
				return "User created successfully";
		}
		catch(Exception e) {
			return "User creation unsuccessful";
		}
	}
	@RequestMapping(value = "/users/update/{id}", method = RequestMethod.POST)
	public String updateUser(@RequestBody User user, @PathVariable("id") int id) {
		try {
				userService.update(user, user.getId());
				return "User updated";
		}catch(Exception e) {
			return "User update failed";
		}
				 
	}
	@RequestMapping(value = "/users/delete/{id}", method = RequestMethod.DELETE)
	public String deleteUser(@PathVariable("id") int id) {
		if(userService.findById(id)!=null) {
			try {
				userService.delete(id);
				return "User deleted";
		}catch(Exception e) {
			return "User delete failed";
		}
			
	}
		return "User not found";
	}
}

