package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.entities.Brand;
import spring.demo.entities.Category;
import spring.demo.entities.Product;
import spring.demo.services.ProductService;

@RestController
public class ProductController {
	@Autowired
	private ProductService productService;
	
	
	 @RequestMapping(value = "/employee/product/create", method = RequestMethod.POST)
	    public HttpStatus createProduct(@RequestBody Product product) {
		 if(productService.findByType(product.getType())!=null) {
			return HttpStatus.BAD_REQUEST;
		}
		 productService.create(product);
		return HttpStatus.OK;
	    }
	 @RequestMapping(value = "/employee/product/get/{id}", method = RequestMethod.GET)
	    public Product createProduct(@PathVariable("id")int id) {
		return productService.findById(id);
	    }
	 
	    @RequestMapping(value = "/employee/product/update/{id}", method = RequestMethod.POST)
	    public String updateProduct(@RequestBody Product product, @PathVariable("id")int id) {
	    	productService.update(product, id);
	    	return "Product updated";
	    }

	    
	    
	    @RequestMapping(value = "/employee/product/delete/{id}", method = RequestMethod.POST)
	    public void deleteProduct(@PathVariable("id") int id) {
	        productService.delete(id);
	    }
	    
	    
	    @RequestMapping(value = "/employee/brand/create", method = RequestMethod.POST)
	    public String createBrand(@RequestBody Brand brand) {
	        try {
	            productService.createBrand(brand);
	            return "Brand created";
	        }
	        catch(Exception e1)
	        {
	            return "Brand was not created";
	        }
	    }
	    @RequestMapping(value = "/employee/category/create", method = RequestMethod.POST)
	    public String createCategoryForm(@RequestBody Category category) {
	    	 try {
		            productService.createCategory(category);
		            return "Category created";
		        }
		        catch(Exception e1)
		        {
		            return "Category was not created";
		        }
	    }
	    @RequestMapping(value = "/search/{searchString}", method = RequestMethod.GET)
	    public List<Product> Search(@PathVariable("searchString") String searchString) {
	    		List<Product> searchResult=null;
	        if(searchString != null){
	             searchResult = productService.searchProducts(searchString);
	           
	        }

	        return searchResult;
	    }
	    @RequestMapping(value = "/user/filter/{brand}/{category}/{priceMin}/{priceMax}", method=RequestMethod.GET)
	    public List<Product> filterProducts(@PathVariable ("brand") String brand,@PathVariable ("category") String category,
	    		@PathVariable ("priceMin") float priceMin, @PathVariable ("priceMax") float priceMax) {
	    	return productService.filter(brand, category, priceMin, priceMax);
	    	
	    }
}
