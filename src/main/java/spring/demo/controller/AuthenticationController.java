package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.entities.User;
import spring.demo.services.UserService;

@RestController
public class AuthenticationController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes="application/x-www-form-urlencoded", produces="application/json")
	public User login(@RequestParam(value="username") String username, @RequestParam(value="password") String password) {
		User user=new User();
		try{
			user=userService.findByUsername(username);
			if(password.compareTo(user.getPassword())!=0) {
				return user;
			}
		}
		catch(Exception e){
			return user;
		}
		return user;
	}

}
