package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.entities.Product;
import spring.demo.services.WishListService;


@RestController
public class WishListController {
	@Autowired
	private WishListService wishListService;
	
	@RequestMapping(value = "/wishlist/add/{userid}/{prodid}", method = RequestMethod.POST)
	public void addToWishList( @PathVariable("userid") int userId, @PathVariable("prodid") int prodId) {
		wishListService.addToWishlist(prodId, userId);
	}
	@RequestMapping(value="/wishlist/delete/{userid}/{prodid}", method=RequestMethod.POST)
	public void deleteFromWishList( @PathVariable("userid") int userId, @PathVariable("prodid") int prodId){
		wishListService.removeFromWishlist(prodId, userId);
	}
	@RequestMapping(value="/wishlist/get/{userId}", method=RequestMethod.GET)
	public List<Product> getWishList(@PathVariable("userId") int userId){
		return wishListService.getWishlist(userId);
	}
}
