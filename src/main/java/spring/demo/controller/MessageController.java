package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.entities.Message;
import spring.demo.services.MessageService;
import spring.demo.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
public class MessageController {
	@Autowired
	private MessageService messageService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/message/send", method = RequestMethod.POST)
	public String sendMessage(@RequestBody Message message){
		if(userService.findByUsername(message.getUsername())==null) {
			return "Username does not exist!";
		}
		messageService.sendMessage(message);
		return "Message sent!";
	}
	@RequestMapping(value = "/message/all/{username}", method = RequestMethod.GET)
	public List<Message> getAllMessagesForUser(@PathVariable("username") String username){
		return messageService.getAllMessagesForUser(username);
	}
	@RequestMapping(value = "/message/delete/{id}", method = RequestMethod.POST)
	public void deleteMessage(@PathVariable("id") int id){
		messageService.deleteMessage(id);
	}
}
