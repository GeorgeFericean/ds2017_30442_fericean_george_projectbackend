package spring.demo.entities;


import javax.persistence.*;

/**
 * Created by George on 5/26/2017.
 */
@Entity
@Table(name="wishlists")
public class Wishlist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int userId;
    private int productId;

    public Wishlist(int userId, int productId) {
        this.userId = userId;
        this.productId = productId;
    }
    public Wishlist()
    {
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
