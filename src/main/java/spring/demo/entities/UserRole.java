package spring.demo.entities;

import javax.persistence.*;

/**
 * Created by George on 4/22/2017.
 */
@Entity
@Table(name = "user_roles")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_role_id")
    private int id;
    @OneToOne
    @JoinColumn(name="username",nullable=false,unique=true,referencedColumnName = "username")
    private User user;
    private String role;
    public UserRole()
    {}
    public UserRole(User user, String role)
    {
        this.user=user;
        this.role=role;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
