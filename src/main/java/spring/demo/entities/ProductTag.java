package spring.demo.entities;

import javax.persistence.*;

/**
 * Created by George on 5/23/2017.
 */
@Entity
@Table(name="tags")
public class ProductTag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int product;
    private String tags;

    public ProductTag() {
    }
   public ProductTag(int product,String tags)
   {
       this.product=product;
       this.tags=tags;
   }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }
}
