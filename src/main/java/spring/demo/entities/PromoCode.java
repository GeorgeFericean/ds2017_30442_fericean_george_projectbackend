package spring.demo.entities;


import javax.persistence.*;

/**
 * Created by George on 5/23/2017.
 */
@Entity
@Table(name="promo_codes")
public class PromoCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    @Column(unique=true)
    private String code;
    private float discount;
    public PromoCode(String username, String code, float discount) {
        this.username = username;
        this.code = code;
        this.discount=discount;
    }
    public PromoCode(){}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
