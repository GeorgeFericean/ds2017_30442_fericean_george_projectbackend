package spring.demo.entities;

import javax.persistence.*;

/**
 * Created by George on 5/30/2017.
 */
@Entity
@Table(name="shopping_carts")
public class ShoppingCart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int userId;
    private int productId;
    private long productQuantity;

    public ShoppingCart(int userId, int productId, int productQuantity) {
        this.userId = userId;
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
    public ShoppingCart()
    {
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(long productQuantity) {
        this.productQuantity = productQuantity;
    }
}
